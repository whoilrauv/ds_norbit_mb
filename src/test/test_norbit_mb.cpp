/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivandor on 08/23/19.
//

#include <ds_util/ds_util.h>
#include <gtest/gtest.h>
#include <ros/param.h>
#include <ds_norbit_mb/norbitmb.h>

///////////////////////////////////////////////////////////////////////////////
// Norbit
///////////////////////////////////////////////////////////////////////////////

class NorbitMBDriverTest : public::testing::Test
{
public:
    std::string pass_path = ros::param::param<std::string>("pass_path", "norbitmb_hex_parse.BIN");

    static void SetUpTestCase()
    {
        ros::Time::init();
    }
};

TEST_F(NorbitMBDriverTest, alwaysPass)
{
    EXPECT_TRUE(true);
}
TEST_F(NorbitMBDriverTest, parsePass) {
    ds_norbit_mb::NorbitMBDriver nh;
    //auto byte_msg = buildRaw(norbitmb_test_data::pass);
    long long int index = 0;
        auto pass = true;
        auto size = 5232;
        //auto size = 1684109404;
        auto byte_msg = ds_core_msgs::RawData{};
        bool ok = false;
        byte_msg.data.resize(size);
        uint8_t foo[size];
        FILE* file = fopen(pass_path.c_str(), "r");
        if (file != nullptr) {
            fseek(file, 0, SEEK_SET);
            //size = ftell(file);
            //rewind(file);
            //fseek(file, index, SEEK_SET);
            ROS_ERROR_STREAM("buffer size: "<<size);
            auto result = fread(&foo, 1, size, file);
            if (result != size) {fputs("Reading Error", stderr); exit (3);}
            fclose(file);
            for (size_t i = 0; i<size; i++) {
                byte_msg.data[i] = foo[i];
            }
            byte_msg.ds_header.io_time = ros::Time::now();
            //ROS_ERROR_STREAM(byte_msg.data.data());
            //ok = nh.parseHeader(byte_msg);
        }
        //Check a couple of variables to make sure byte msg is okay
        EXPECT_EQ('e', byte_msg.data[0]);
        //EXPECT_EQ(pass, ok);
        EXPECT_EQ('7', byte_msg.data[18]);
        EXPECT_EQ('f', byte_msg.data[39]);
    }

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "test_norbitmb_parse");
  auto ret = RUN_ALL_TESTS();
  ros::shutdown();
  return ret;
}
